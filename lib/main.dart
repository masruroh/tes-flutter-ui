import 'package:flutter/material.dart';
import 'package:tes_flutter_ui/ui/page/homepage.dart';
import 'package:tes_flutter_ui/ui/part/style.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tes Flutter UI Ordo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        iconTheme: IconThemeData(color: putih, size: sizeIcon),
        scaffoldBackgroundColor: bg,
      ),
      home: HomePage(),
    );
  }
}
