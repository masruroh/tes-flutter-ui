import 'package:flutter/material.dart';

double jarak = 10.0;
double rad = 10.0;
double elev = 5.0;
double sizeIcon = 16;
Color bg = Color(0XFFF9F9F9);
Color hitam = Colors.black,
    putih = Colors.white,
    abu = Colors.grey,
    kuning = Colors.yellow,
    orange = Color(0XFFFF9A00),
    biru = Color(0XFF53B2FC),
    birutua = Color(0XFF127CCE),
    hijau = Color(0XFF41BE06),
    hijaumuda = Color(0XFF3AB648),
    hijautua = Color(0XFF526430),
    abuIcon = Color(0XFFC6C4C4),

    // putihGelap = Color(0xffe5e5e5),
    putihGelap = Colors.white70;
double fullwidth, fullheight;
List<double> size = [15, 17, 20, 25];
String lorem =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ullamco laboris nisi ut aliquip ex ea commodo .';
style(double size,
    {Color color = Colors.black, bool i = false, bool b = false}) {
  return TextStyle(
      color: color,
      fontSize: size,
      fontStyle: (i) ? FontStyle.italic : FontStyle.normal,
      fontWeight: (b) ? FontWeight.bold : FontWeight.normal);
}

roundC(Color color, {double rad2}) {
  return BoxDecoration(
    borderRadius: BorderRadius.circular((rad2 == null) ? rad : rad2),
    color: color,
  );
}

tombol(String title, Function onpressed,
    {Color colortext = Colors.black, double width = 100, Color colorbg}) {
  return Container(
      child: MaterialButton(
          child: Text(title, style: style(size[0], color: colortext, b: true)),
          color: (colorbg == null) ? putih : colorbg,
          minWidth: width,
          onPressed: onpressed));
}

tombolModif(String title, Function onpressed,
    {Color colortext = Colors.black,
    Color colorbg,
    double padd = 10,
    double horipadd = 10,
    double vertpadd = 10}) {
  return GestureDetector(
    onTap: onpressed,
    child: Container(
        padding: EdgeInsets.fromLTRB(vertpadd, horipadd, vertpadd, horipadd),
        child: Text(title, style: style(size[0], color: colortext, b: true)),
        decoration: roundC((colorbg == null) ? putih : colorbg)),
  );
}

linGradient(List<Color> colors) {
  return LinearGradient(
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
    colors: colors,
  );
}
