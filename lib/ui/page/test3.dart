import 'package:flutter/material.dart';
import 'package:tes_flutter_ui/ui/part/style.dart';

class Test3Page extends StatefulWidget {
  @override
  _Test3PageState createState() => _Test3PageState();
}

class _Test3PageState extends State<Test3Page> {
  int _selectedIndex = 0;

  contDataBonus(String title, String content) {
    return Material(
      elevation: elev,
      borderRadius: BorderRadius.circular(rad),
      child: Container(
        padding: EdgeInsets.all(jarak * 2),
        width: double.infinity,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(title, style: style(size[0], b: true, color: orange)),
          Text(content, style: style(size[3], b: true)),
        ]),
      ),
    );
  }

  contListRebate(String id, String rebate, String tgl) {
    return Container(
      margin: EdgeInsets.all(jarak),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('ID Transaksi', style: style(size[0], color: abu, b: true)),
              Text(id, style: style(size[0], b: true)),
            ],
          ),
        ),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Rebate', style: style(size[0], color: abu, b: true)),
              Text(rebate, style: style(size[0], color: hijau, b: true)),
            ],
          ),
        ),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Tanggal', style: style(size[0], color: abu, b: true)),
              Text(tgl, style: style(size[0], color: hijau, b: true)),
            ],
          ),
        ),
      ]),
    );
  }

  contHistRebate(String id, String rebate, String tgl) {
    return Container(
      // color: biru,
      margin: EdgeInsets.all(jarak),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Container(
          child: Row(children: [
            // icon leading
            Container(
              padding: EdgeInsets.all(jarak / 1.5),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                gradient: linGradient([birutua, biru]),
              ),
              child: Icon(Icons.receipt, size: sizeIcon + 3),
            ),
            SizedBox(width: jarak),
            // isi data
            Column(children: [
              Text(id, style: style(size[1], b: true)),
              Text(tgl, style: style(size[0], color: abu)),
            ]),
          ]),
        ),
        Container(
            padding: EdgeInsets.fromLTRB(jarak, jarak / 2, jarak, jarak / 2),
            decoration: roundC(orange, rad2: rad * 2),
            child: Text(rebate, style: style(size[0], color: putih))),
      ]),
    );
  }

  iconBellWithNotif() {
    return Container(
      width: sizeIcon * 1.5,
      child: Stack(
        children: [
          Align(
            alignment: Alignment(0, 0),
            child:
                Icon(Icons.notifications, color: birutua, size: sizeIcon * 1.5),
          ),
          Align(
            alignment: Alignment(0.8, -0.3),
            child: Container(
                width: sizeIcon - 3,
                height: sizeIcon - 3,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: orange,
                    border: Border.all(color: putih, width: 2))),
          ),
        ],
      ),
    );
  }

  tombolComplaint() {
    return Material(
      elevation: elev,
      borderRadius: BorderRadius.circular(rad),
      child: Container(
        padding: EdgeInsets.fromLTRB(jarak * 2, jarak, jarak * 2, jarak),
        height: 50,
        width: double.infinity,
        // decoration: roundC(Colors.blue),
        decoration: BoxDecoration(
          color: biru,
          borderRadius: BorderRadius.circular(rad),
          gradient: linGradient([birutua, biru]),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(),
            Text('Complain', style: style(size[2], color: putih, b: true)),
            Icon(Icons.arrow_forward, color: putih),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: putihGelap,
          leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Container(
              margin: EdgeInsets.only(left: jarak),
              decoration: BoxDecoration(shape: BoxShape.circle, color: orange),
              child: Icon(Icons.arrow_back, size: sizeIcon),
            ),
          ),
          leadingWidth: sizeIcon / 2 + jarak * 2.5,
          title: Text('FINANSIAL', style: style(size[1], b: true)),
          actions: [
            Icon(Icons.assessment, color: birutua, size: sizeIcon * 1.5),
            SizedBox(width: jarak),
            // Icon(Icons.notifications, color: birutua, size: sizeIcon * 1.5),
            iconBellWithNotif(),
            SizedBox(width: jarak),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: ''),
            BottomNavigationBarItem(
                icon: Icon(Icons.event_note_outlined), label: ''),
            BottomNavigationBarItem(icon: Icon(Icons.history), label: ''),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: abu,
          showSelectedLabels: false,
          unselectedItemColor: abu,
          onTap: (value) {},
        ),
        body: Container(
            padding: EdgeInsets.all(jarak),
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.all(jarak / 2),
                    child: Column(children: [
                      contDataBonus('TOTAL BONUS', 'Rp 5.000.000,00'),
                      SizedBox(height: jarak),
                      contDataBonus('PENDING BONUS', 'Rp 50.000,00'),
                      SizedBox(height: jarak),

                      // Daftar Rebate
                      Container(
                        height: 250,
                        child: Material(
                          elevation: elev,
                          borderRadius: BorderRadius.circular(rad),
                          child: Column(
                            children: [
                              // header rebate
                              Container(
                                padding: EdgeInsets.all(jarak),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Daftar Rebate',
                                        style: style(size[0])),
                                    Row(
                                      children: [
                                        // all
                                        Container(
                                          padding: EdgeInsets.all(jarak / 2),
                                          width: 70,
                                          decoration: roundC(orange),
                                          child: Text('Semua',
                                              style:
                                                  style(size[0], color: putih),
                                              textAlign: TextAlign.center),
                                        ),
                                        SizedBox(width: jarak),
                                        // filter
                                        Container(
                                          padding: EdgeInsets.all(jarak / 2),
                                          width: 70,
                                          decoration: roundC(biru),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text('Filter',
                                                  style: style(size[0],
                                                      color: putih),
                                                  textAlign: TextAlign.center),
                                              SizedBox(width: jarak / 2),
                                              // Icon(Icons.sort),
                                              Image.asset(
                                                  'assets/icons/options.png'),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),

                              // pemisah header dan body
                              Container(
                                  height: 1,
                                  width: double.infinity,
                                  color: hitam),

                              // body rebate
                              Flexible(
                                child: ListView(children: [
                                  contListRebate('#EC1201211', 'Rp. 150.000',
                                      '14 Juli 2021'),
                                  contListRebate('#EC1201211', 'Rp. 150.000',
                                      '15 Juli 2021'),
                                  contListRebate('#EC1201211', 'Rp. 150.000',
                                      '16 Juli 2021'),
                                  contListRebate('#EC1201211', 'Rp. 150.000',
                                      '17 Juli 2021'),
                                  contListRebate('#EC1201211', 'Rp. 150.000',
                                      '17 Juli 2021'),
                                  contListRebate('#EC1201211', 'Rp. 150.000',
                                      '18 Juli 2021'),
                                  contListRebate('#EC1201211', 'Rp. 150.000',
                                      '18 Juli 2021'),
                                ]),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: jarak),

                      // Riwayat Rebate
                      Container(
                        height: 250,
                        child: Material(
                          elevation: elev,
                          borderRadius: BorderRadius.circular(rad),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              // header rebate
                              Container(
                                padding: EdgeInsets.all(jarak),
                                child: Text('Riwayat Rebate',
                                    style:
                                        style(size[1], b: true, color: orange)),
                              ),

                              // body rebate
                              Flexible(
                                child: ListView(children: [
                                  contHistRebate('#EC1201211', 'Rp. 150.000',
                                      '14 Juli 2021'),
                                  contHistRebate('#EC1201211', 'Rp. 150.000',
                                      '15 Juli 2021'),
                                  contHistRebate('#EC1201211', 'Rp. 150.000',
                                      '16 Juli 2021'),
                                  contHistRebate('#EC1201211', 'Rp. 150.000',
                                      '17 Juli 2021'),
                                  contHistRebate('#EC1201211', 'Rp. 150.000',
                                      '17 Juli 2021'),
                                  contHistRebate('#EC1201211', 'Rp. 150.000',
                                      '18 Juli 2021'),
                                  contHistRebate('#EC1201211', 'Rp. 150.000',
                                      '18 Juli 2021'),
                                ]),
                              ),
                            ],
                          ),
                        ),
                      )
                    ]),
                  ),
                ),
                Align(alignment: Alignment(0, 0.9), child: tombolComplaint())
              ],
            )),
      ),
    );
  }
}
