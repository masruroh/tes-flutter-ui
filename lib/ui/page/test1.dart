import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:tes_flutter_ui/ui/part/style.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class Test1Page extends StatefulWidget {
  @override
  _Test1PageState createState() => _Test1PageState();
}

class _Test1PageState extends State<Test1Page> {
  int _selectedIndex = 0;

  double sizeFoto = 50;
  circleProfil(double size, Color color) {
    return Container(
      height: size,
      width: size,
      margin: EdgeInsets.only(left: 8, bottom: jarak / 1.5),
      child: Material(
        elevation: 3,
        color: color,
        borderRadius: BorderRadius.circular(rad),
      ),
    );
  }

  contPesanan(
      String nama, String berat, String harga, String jml, String note) {
    return Container(
      padding: EdgeInsets.only(top: jarak / 2),
      child: Column(children: [
        // foto item + nama item + harga + jml
        Row(
          children: [
            Material(
              elevation: elev,
              borderRadius: BorderRadius.circular(100),
              child: Container(
                margin: EdgeInsets.all(jarak),
                width: sizeFoto,
                height: sizeFoto,
                child: Image.asset('assets/buah.png', fit: BoxFit.fill),
              ),
            ),
            SizedBox(width: jarak),
            Flexible(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(nama, style: style(size[1], b: true)),
                    SizedBox(height: 2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(berat, style: style(size[0], color: abu)),
                        Container(),
                        Text(jml, style: style(size[0], b: true)),
                        Container(),
                      ],
                    ),
                    SizedBox(height: 2),
                    Text(harga, style: style(size[1], b: true, color: hijautua))
                  ]),
            ),
          ],
        ),
        SizedBox(height: jarak * 1.5),
        // catatan item
        Container(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Catatan Item', style: style(size[0], b: true)),
              SizedBox(height: jarak / 2),
              Text(note,
                  style: style(size[0], color: abu),
                  textAlign: TextAlign.justify),
            ],
          ),
        ),

        // divider
        Divider(thickness: 1.5),
      ]),
    );
  }

  iconWithNotif() {
    return Container(
      width: sizeIcon * 1.5,
      child: Stack(
        children: [
          Align(
            alignment: Alignment(0, 0),
            child: Icon(Icons.chat_bubble, size: sizeIcon * 1.5),
          ),
          Align(
            alignment: Alignment(0.8, -0.3),
            child: Container(
                width: sizeIcon - 3,
                height: sizeIcon - 3,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: putih,
                    border: Border.all(color: hijautua, width: 3))),
          ),
        ],
      ),
    );
  }

  _panel(ScrollController sc) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(
          controller: sc,
          children: <Widget>[
            SizedBox(height: jarak),
            // garis hijau diatas
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Container(
                width: fullwidth / 5,
                height: 5,
                decoration: BoxDecoration(
                    color: hijautua,
                    borderRadius: BorderRadius.all(Radius.circular(rad))),
              ),
            ]),
            SizedBox(height: jarak * 2),
            // isi
            Container(
              // margin: EdgeInsets.only(top: 50),
              decoration: roundC(putih, rad2: rad * 2),
              padding: EdgeInsets.all(jarak),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // photo profil + nama + icon chat
                  Container(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // profil
                          Row(children: [
                            Container(
                              width: sizeFoto + jarak,
                              height: sizeFoto + jarak,
                              child: Stack(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(jarak / 1.2),
                                    child: Material(
                                      elevation: elev * 2,
                                      borderRadius: BorderRadius.circular(100),
                                      child: Container(
                                        child: Image.asset('assets/photo3.png',
                                            width: sizeFoto, height: sizeFoto),
                                      ),
                                    ),
                                  ),
                                  SfRadialGauge(axes: <RadialAxis>[
                                    RadialAxis(
                                      startAngle: 90,
                                      endAngle: 325,
                                      showLabels: false,
                                      showTicks: false,
                                      axisLineStyle: AxisLineStyle(
                                        thickness: 0.15,
                                        cornerStyle: CornerStyle.bothCurve,
                                        thicknessUnit: GaugeSizeUnit.factor,
                                        gradient: SweepGradient(
                                          colors: <Color>[hijautua, hijaumuda],
                                          stops: <double>[0.33, 0.73],
                                        ),
                                      ),
                                    ),
                                  ]),
                                ],
                              ),
                            ),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('David Morel',
                                      style: style(size[1], b: true)),
                                  SizedBox(height: jarak / 2),
                                  Text('B 1201 FA',
                                      style: style(size[0],
                                          color: hijau, b: true)),
                                ])
                          ]),
                          Material(
                            elevation: elev * 2,
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              width: 60,
                              height: 60,
                              child: Stack(
                                children: [
                                  Image.asset('assets/icons/circle.png',
                                      width: 60, height: 60),
                                  Center(
                                    child: Image.asset('assets/icons/chat.png',
                                        width: sizeIcon * 1.5,
                                        height: sizeIcon * 1.5),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ]),
                  ),
                  SizedBox(height: jarak * 2),

                  // status dan alamat
                  Container(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // status
                          Row(children: [
                            Image.asset('assets/icons/clock.png'),
                            SizedBox(width: jarak),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Status', style: style(size[0])),
                                Text('Sedang mengambil barang',
                                    style: style(size[0], b: true)),
                              ],
                            )
                          ]),

                          // jarak
                          Container(
                            margin: EdgeInsets.only(
                                top: jarak / 3, bottom: jarak / 3),
                            child: Column(children: [
                              circleProfil(5, Colors.grey[200]),
                              circleProfil(6, Colors.grey[300]),
                              circleProfil(7, Colors.grey[400]),
                            ]),
                          ),

                          // alamat
                          Row(children: [
                            Image.asset('assets/icons/pin.png'),
                            SizedBox(width: jarak),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Alamat Anda', style: style(size[0])),
                                Text('Taman Indah Dago No. 612',
                                    style: style(size[0], b: true)),
                              ],
                            )
                          ]),
                        ]),
                  ),

                  // pesanan
                  Container(
                    margin: EdgeInsets.only(top: jarak * 2, bottom: jarak),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Pesanan', style: style(size[1], b: true)),
                        SizedBox(height: jarak),
                        contPesanan('Strawberry', '100 Gram', 'Rp 75.000',
                            '2 item', lorem),
                        contPesanan(
                            'Nanas', '100 Gram', 'Rp 75.000', '2 item', lorem),

                        SizedBox(height: jarak),
                        // catatan pesanan
                        Container(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Catatan Pesanan',
                                  style: style(size[1], b: true)),
                              SizedBox(height: jarak / 2),
                              Text(lorem,
                                  style: style(size[0], color: abu),
                                  textAlign: TextAlign.justify),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  _map() {
    String url = "https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png";

    return FlutterMap(
      options: MapOptions(
        center: LatLng(51.416005, -0.074137),
        zoom: 14.0,
        maxZoom: 15,
      ),
      layers: [
        TileLayerOptions(urlTemplate: url),
        MarkerLayerOptions(markers: [
          Marker(
              point: LatLng(51.42143366474916, -0.0679062866309534),
              builder: (ctx) => Container(
                    padding: EdgeInsets.all(jarak / 1.3),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color(0XFFEB5757).withOpacity(0.1),
                    ),
                    child: Container(
                        decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: hijautua,
                    )),
                  ),
              height: 60),
          // driver
          Marker(
              point: LatLng(51.4191983349635, -0.07395882365638051),
              builder: (ctx) => CircleAvatar(
                    backgroundColor: Color(0XFF92D274).withOpacity(0.8),
                    radius: 50,
                    child: Image.asset('assets/icons/bike.png',
                        height: sizeIcon + 4,
                        width: sizeIcon + 4,
                        fit: BoxFit.cover),
                  ),
              height: 60),
          Marker(
              point: LatLng(51.41844966470293, -0.08133878907301689),
              builder: (ctx) => Image.asset('assets/icons/loc.png'),
              height: 60)
        ]),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: putihGelap,
        leading: GestureDetector(
          onTap: () => Navigator.of(context).pop(),
          child: Container(
            margin: EdgeInsets.only(left: jarak),
            decoration: BoxDecoration(shape: BoxShape.circle, color: hijautua),
            child: Icon(Icons.arrow_back, size: sizeIcon),
          ),
        ),
        leadingWidth: sizeIcon / 2 + jarak * 2.5,
        title: Text('LACAK PESANAN', style: style(size[1], b: true)),
        actions: [
          Image.asset('assets/icons/bag.png',
              width: sizeIcon * 1.5, height: sizeIcon * 1.5),
          SizedBox(width: jarak),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.assignment, color: abuIcon), label: ''),
          BottomNavigationBarItem(
              icon: Icon(Icons.swap_horiz, color: abuIcon), label: ''),
          BottomNavigationBarItem(
              icon: Icon(Icons.home, color: abuIcon), label: ''),
          BottomNavigationBarItem(
              icon: Icon(Icons.notifications_none, color: abuIcon), label: ''),
          BottomNavigationBarItem(
              icon: Icon(Icons.person_outline, color: abuIcon), label: ''),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: abu,
        showSelectedLabels: false,
        unselectedItemColor: abu,
        onTap: (value) {},
      ),
      body: SlidingUpPanel(
        maxHeight: fullheight * 0.4,
        minHeight: 125,
        parallaxEnabled: true,
        parallaxOffset: .5,
        body: _map(),
        panelBuilder: (sc) => _panel(sc),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(rad * 2),
            topRight: Radius.circular(rad * 2)),
      ),
    ));
  }
}
