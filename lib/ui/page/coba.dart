import 'dart:ui';

import 'package:flutter/material.dart';
// import 'package:flutter_map/flutter_map.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:tes_flutter_ui/ui/part/style.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class CobaPage extends StatefulWidget {
  @override
  _CobaPageState createState() => _CobaPageState();
}

class _CobaPageState extends State<CobaPage> {
  double sizeFoto = 50;
  circleProfil(double size, Color color) {
    return Container(
      height: size,
      width: size,
      margin: EdgeInsets.only(left: 3, bottom: jarak / 1.5),
      child: Material(
        elevation: 3,
        color: color,
        borderRadius: BorderRadius.circular(rad),
      ),
    );
  }

  contPesanan(
      String nama, String berat, String harga, String jml, String note) {
    return Container(
      padding: EdgeInsets.only(top: jarak / 2),
      child: Column(children: [
        // foto item + nama item + harga + jml
        Row(
          children: [
            Material(
              elevation: elev,
              borderRadius: BorderRadius.circular(100),
              child: Container(
                margin: EdgeInsets.all(jarak),
                width: sizeFoto,
                height: sizeFoto,
                child: Image.asset('assets/buah.png', fit: BoxFit.fill),
              ),
            ),
            SizedBox(width: jarak),
            Flexible(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(nama, style: style(size[1], b: true)),
                    SizedBox(height: 2),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(berat, style: style(size[0], color: abu)),
                        Container(),
                        Text(jml, style: style(size[0], b: true)),
                        Container(),
                      ],
                    ),
                    SizedBox(height: 2),
                    Text(harga, style: style(size[1], b: true, color: hijautua))
                  ]),
            ),
          ],
        ),
        SizedBox(height: jarak * 1.5),
        // catatan item
        Container(
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Catatan Item', style: style(size[0], b: true)),
              SizedBox(height: jarak / 2),
              Text(note,
                  style: style(size[0], color: abu),
                  textAlign: TextAlign.justify),
            ],
          ),
        ),

        // divider
        Divider(thickness: 1.5),
      ]),
    );
  }

  _panel(ScrollController sc) {
    return MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: ListView(
          controller: sc,
          children: <Widget>[
            SizedBox(height: jarak),
            // garis hijau diatas
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              Container(
                width: fullwidth / 5,
                height: 5,
                decoration: BoxDecoration(
                    color: hijautua,
                    borderRadius: BorderRadius.all(Radius.circular(rad))),
              ),
            ]),
            SizedBox(height: jarak * 2),
            // isi
            Container(
              // margin: EdgeInsets.only(top: 50),
              decoration: roundC(putih, rad2: rad * 2),
              padding: EdgeInsets.all(jarak),
              child: Column(
                // crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // photo profil + nama + icon chat
                  Container(
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // profil
                          Row(children: [
                            Container(
                              width: sizeFoto + jarak,
                              height: sizeFoto + jarak,
                              child: Stack(
                                children: [
                                  Container(
                                    padding: EdgeInsets.all(jarak / 1.2),
                                    child: Material(
                                      elevation: elev * 2,
                                      borderRadius: BorderRadius.circular(100),
                                      child: Container(
                                        child: Image.asset('assets/photo3.png',
                                            width: sizeFoto, height: sizeFoto),
                                      ),
                                    ),
                                  ),
                                  SfRadialGauge(axes: <RadialAxis>[
                                    RadialAxis(
                                      startAngle: 90,
                                      endAngle: 325,
                                      showLabels: false,
                                      showTicks: false,
                                      axisLineStyle: AxisLineStyle(
                                        thickness: 0.15,
                                        cornerStyle: CornerStyle.bothCurve,
                                        color: hijautua,
                                        thicknessUnit: GaugeSizeUnit.factor,
                                      ),
                                    ),
                                  ]),
                                ],
                              ),
                            ),
                            Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('David Morel',
                                      style: style(size[1], b: true)),
                                  SizedBox(height: jarak / 2),
                                  Text('B 1201 FA',
                                      style: style(size[0],
                                          color: hijau, b: true)),
                                ])
                          ]),
                          Material(
                            elevation: elev * 2,
                            color: hijautua,
                            borderRadius: BorderRadius.circular(100),
                            child: Container(
                              padding: EdgeInsets.all(jarak * 1.5),
                              child: Icon(Icons.chat, size: 30),
                            ),
                          ),
                        ]),
                  ),
                  SizedBox(height: jarak * 2),

                  // status dan alamat
                  Container(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // status
                          Row(children: [
                            Icon(Icons.lock_clock, color: hijau, size: 20),
                            SizedBox(width: jarak),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Status', style: style(size[0])),
                                Text('Sedang mengambil barang',
                                    style: style(size[0], b: true)),
                              ],
                            )
                          ]),

                          // jarak
                          Container(
                            margin: EdgeInsets.only(
                                top: jarak / 3, bottom: jarak / 3),
                            child: Column(children: [
                              circleProfil(5, Colors.grey[200]),
                              circleProfil(6, Colors.grey[300]),
                              circleProfil(7, Colors.grey[400]),
                            ]),
                          ),

                          // alamat
                          Row(children: [
                            Icon(Icons.lock_clock, color: hijau, size: 20),
                            SizedBox(width: jarak),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Alamat Anda', style: style(size[0])),
                                Text('Taman Indah Dago No. 612',
                                    style: style(size[0], b: true)),
                              ],
                            )
                          ]),
                        ]),
                  ),

                  // pesanan
                  Container(
                    margin: EdgeInsets.only(top: jarak * 2, bottom: jarak),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Pesanan', style: style(size[1], b: true)),
                        SizedBox(height: jarak),
                        contPesanan('Strawberry', '100 Gram', 'Rp 75.000',
                            '2 item', lorem),
                        contPesanan(
                            'Nanas', '100 Gram', 'Rp 75.000', '2 item', lorem),

                        SizedBox(height: jarak),
                        // catatan pesanan
                        Container(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Catatan Pesanan',
                                  style: style(size[1], b: true)),
                              SizedBox(height: jarak / 2),
                              Text(lorem,
                                  style: style(size[0], color: abu),
                                  textAlign: TextAlign.justify),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: SlidingUpPanel(
        maxHeight: fullheight * 0.4,
        minHeight: 125,
        parallaxEnabled: true,
        parallaxOffset: .5,
        body: Image.asset('assets/bunga3.png',
            height: fullheight, width: fullwidth, fit: BoxFit.cover),
        panelBuilder: (sc) => _panel(sc),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(rad * 2),
            topRight: Radius.circular(rad * 2)),
      ),
    ));
  }
}
