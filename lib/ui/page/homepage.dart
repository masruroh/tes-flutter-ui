import 'package:flutter/material.dart';
import 'package:tes_flutter_ui/ui/part/style.dart';
import 'package:tes_flutter_ui/ui/page/test1.dart';
import 'package:tes_flutter_ui/ui/page/test2.dart';
import 'package:tes_flutter_ui/ui/page/test3.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  contData({String title, Widget page}) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context, MaterialPageRoute(builder: (context) => page)),
      child: Container(
        padding: EdgeInsets.all(jarak * 1.5),
        margin: EdgeInsets.only(bottom: jarak * 2),
        width: double.infinity,
        decoration: roundC(hitam, rad2: rad * 2),
        child: Text(title,
            style: style(size[2], color: putih), textAlign: TextAlign.center),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      fullheight = MediaQuery.of(context).size.height;
      fullwidth = MediaQuery.of(context).size.width;
    });
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Container(
              margin: EdgeInsets.only(left: jarak * 3, right: jarak * 3),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Made by', style: style(size[0])),
                  SizedBox(height: jarak * 2),
                  Text('[Izzatul Masruroh]', style: style(size[3], b: true)),
                  SizedBox(height: jarak * 4),
                  contData(title: 'Test 1', page: Test1Page()),
                  contData(title: 'Test 2', page: Test2Page()),
                  contData(title: 'Test 3', page: Test3Page()),
                ],
              )),
        ),
      ),
    );
  }
}
