import 'package:flutter/material.dart';
import 'package:tes_flutter_ui/ui/part/style.dart';

class Test2Page extends StatefulWidget {
  @override
  _Test2PageState createState() => _Test2PageState();
}

class _Test2PageState extends State<Test2Page> {
  int _selectedIndex = 0;
  double sizeFoto = 100;
  contFoto(String path) {
    return Container(
      width: sizeFoto,
      height: sizeFoto,
      margin: EdgeInsets.all(1),
      child: Stack(
        children: [
          Image.asset('assets/' + path, fit: BoxFit.cover),
          Align(
            alignment: Alignment(1, -1),
            child: Container(
              margin: EdgeInsets.all(jarak / 2),
              padding: EdgeInsets.fromLTRB(jarak / 2, 1, jarak / 2, 1),
              decoration: roundC(hitam.withOpacity(0.6)),
              child: Wrap(children: [
                Icon(Icons.star, size: size[0] - 3, color: kuning),
                Text('5.0', style: style(size[0] - 2, color: putih)),
              ]),
            ),
          ),
          // container nama customer
          Align(
            alignment: Alignment(-1, -1),
            child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(jarak / 3),
                  color: hitam.withOpacity(0.4),
                  child: Text('Nama Customer',
                      style: style(size[0] - 2, color: putih, b: true))),
            ]),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      sizeFoto = ((fullwidth - jarak * 4) / 3) - 3;
    });
    print(sizeFoto);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: putihGelap,
          leading: GestureDetector(
            onTap: () => Navigator.of(context).pop(),
            child: Container(
              margin: EdgeInsets.only(left: jarak),
              decoration: BoxDecoration(shape: BoxShape.circle, color: hitam),
              child: Icon(Icons.arrow_back, size: sizeIcon),
            ),
          ),
          leadingWidth: sizeIcon / 2 + jarak * 2.5,
          title: Text('PORTFOLIO VENDOR', style: style(size[1], b: true)),
          actions: [
            Icon(Icons.notifications, color: hitam, size: sizeIcon * 1.5),
            SizedBox(width: jarak),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.chrome_reader_mode, color: abuIcon),
                label: ''),
            BottomNavigationBarItem(
                icon: Icon(Icons.swap_horiz, color: abuIcon), label: ''),
            BottomNavigationBarItem(
                icon: Icon(Icons.move_to_inbox, color: abuIcon), label: ''),
            BottomNavigationBarItem(
                icon: Icon(Icons.assessment, color: abuIcon), label: ''),
            // BottomNavigationBarItem(icon: Icon(Icons.assessment), label: 'dsa'),
            BottomNavigationBarItem(
                icon: Image.asset('assets/icons/racinghelmet.png'), label: '')
            // BottomNavigationBarItem(
            //     icon: Icon(CommunityMaterialIcons.zodiac_taurus),
            //     label: 'dsdfa'),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: abu,
          showSelectedLabels: false,
          unselectedItemColor: abu,
          onTap: (value) {},
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(jarak * 2),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Header profil
                Container(
                  margin: EdgeInsets.only(right: jarak / 2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: abu, width: 1.5),
                        ),
                        padding: EdgeInsets.all(2),
                        child: Image.asset('assets/team2.png',
                            width: 75, height: 75, fit: BoxFit.cover),
                      ),
                      Column(children: [
                        Text('5.0', style: style(size[1], b: true)),
                        Text('Rating', style: style(size[0])),
                      ]),
                      Column(children: [
                        Text('100', style: style(size[1], b: true)),
                        Text('Review', style: style(size[0])),
                      ]),
                      Column(children: [
                        Text('162', style: style(size[1], b: true)),
                        Text('Pesanan', style: style(size[0])),
                      ]),
                    ],
                  ),
                ),

                // keterangan profil
                Container(
                  margin: EdgeInsets.fromLTRB(0, jarak, 0, jarak),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Dina Florist', style: style(size[0], b: true)),
                        SizedBox(height: jarak / 2),
                        Text('Toko Bunga terbaiks se Indonesia Raya',
                            style: style(size[0])),
                        Text('Harga Murah Produk Berkualitas',
                            style: style(size[0])),
                      ]),
                ),

                // tombol portofolio
                Container(
                  padding: EdgeInsets.all(jarak / 2),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(rad / 2),
                    color: putih,
                    border: Border.all(color: abu),
                  ),
                  child: Text(
                    'PORTOFOLIO',
                    style: style(size[0], b: true),
                    textAlign: TextAlign.center,
                  ),
                ),

                // list foto"
                Container(
                  margin: EdgeInsets.only(top: jarak),
                  child: Wrap(
                    children: [
                      contFoto('bunga1.png'),
                      contFoto('bunga3.png'),
                      contFoto('bunga2.png'),
                      //
                      contFoto('bunga2.png'),
                      contFoto('bunga1.png'),
                      contFoto('bunga3.png'),
                      //
                      contFoto('bunga1.png'),
                      contFoto('bunga2.png'),
                      contFoto('bunga3.png'),
                      //
                      contFoto('bunga3.png'),
                      contFoto('bunga1.png'),
                      contFoto('bunga2.png'),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
